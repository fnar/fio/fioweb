﻿using System.Text.Json.Serialization;

namespace FIOWeb.UniverseMap
{
    public class SceneObjectBase
    {
        [JsonPropertyName("isVisible")]
        public bool IsVisible { get; set; } = true;

        [JsonPropertyName("color")]
        public uint Color { get; set; } = 0;

        [JsonPropertyName("dotNetMethodToInvokeOnHover")]
        public string DotNetMethodToInvokeOnHover { get; set; }

        [JsonPropertyName("colorOnHover")]
        public uint ColorOnHover { get; set; } = 0;

        [JsonPropertyName("dotNetMethodToInvokeOnClick")]
        public string DotNetMethodToInvokeOnClick { get; set; }

        [JsonPropertyName("colorOnClick")]
        public uint ColorOnClick { get; set; } = 0;
    }
}
