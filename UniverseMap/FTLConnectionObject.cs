using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class FTLConnectionObject : SceneObjectBase
    {
        [JsonPropertyName("startSystemId")]
        public string StartSystemId { get; set; }
        [JsonPropertyName("startVertices")]
        public List<double> StartVertices { get; set; } = new List<double>(3);

        [JsonPropertyName("endSystemId")]
        public string EndSystemId { get; set; }
        [JsonPropertyName("endVertices")]
        public List<double> EndVertices { get; set; } = new List<double>(3);

        public static Dictionary<string, FTLConnectionObject> GetFTLConnections(List<SystemStarPayload> SystemStars, List<RainSystemLink> SystemLinks)
        {
            var GuidToFTLConnectionObjects = new Dictionary<string, FTLConnectionObject>();
            foreach (var rainSystemLink in SystemLinks)
            {
                var leftStar = SystemStars.Where(s => s.NaturalId == rainSystemLink.Left).FirstOrDefault();
                var rightStar = SystemStars.Where(s => s.NaturalId == rainSystemLink.Right).FirstOrDefault();

                if (leftStar != null && rightStar != null)
                {
                    var key = Guid.NewGuid().ToString();
                    var conn = new FTLConnectionObject();
                    conn.StartSystemId = leftStar.NaturalId;
                    conn.StartVertices.Add(leftStar.PositionX);
                    conn.StartVertices.Add(leftStar.PositionY);
                    conn.StartVertices.Add(leftStar.PositionZ);
                    conn.EndSystemId = rightStar.NaturalId;
                    conn.EndVertices.Add(rightStar.PositionX);
                    conn.EndVertices.Add(rightStar.PositionY);
                    conn.EndVertices.Add(rightStar.PositionZ);
                    conn.Color = 0x818181;

                    GuidToFTLConnectionObjects.Add(key, conn);
                }
            }

            return GuidToFTLConnectionObjects;
        }

        public FTLConnectionObject GetStrippedCopy()
        {
            FTLConnectionObject copy = new FTLConnectionObject();
            copy.IsVisible = this.IsVisible;
            copy.Color = this.Color;
            copy.DotNetMethodToInvokeOnHover = this.DotNetMethodToInvokeOnHover;
            copy.ColorOnHover = this.ColorOnHover;
            copy.DotNetMethodToInvokeOnClick = this.DotNetMethodToInvokeOnClick;
            copy.ColorOnClick = this.ColorOnClick;
            copy.StartSystemId = this.StartSystemId;
            copy.StartVertices = this.StartVertices.Select(sv => sv).ToList();
            copy.EndSystemId = this.EndSystemId;
            copy.EndVertices = this.EndVertices.Select(ev => ev).ToList();


            return copy;
        }
    }
}