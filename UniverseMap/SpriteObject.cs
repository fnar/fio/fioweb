﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace FIOWeb.UniverseMap
{
    public enum SpriteObjectType : int
    {
        CommodityExchange = 0,
        PlayerShipInSystem = 1,
        PlayerShipTraveling = 2,
        Shipyard = 3,
        PlayerStorage = 4,
        PlayerBase = 5,
        LocalMarket = 6,
    }

    public class SpriteObject : SceneObjectBase
    {
        [JsonPropertyName("spriteObjType")]
        public int SpriteObjType { get; set; }

        [JsonPropertyName("vertices")]
        public List<double> Vertices { get; set; } = new List<double>(3);

        [JsonPropertyName("systemNaturalId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string SystemNaturalId { get; set; }

        [JsonPropertyName("addressableId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string AddressableId { get; set; }

        [JsonPropertyName("spriteNaturalIdentifiers")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<string> SpriteNaturalIdentifiers { get; set; } = new List<string>();

        public SpriteObject GetStrippedCopy()
        {
            SpriteObject copy = new SpriteObject();
            copy.IsVisible = this.IsVisible;
            copy.Color = this.Color;
            copy.DotNetMethodToInvokeOnHover = this.DotNetMethodToInvokeOnHover;
            copy.ColorOnHover = this.ColorOnHover;
            copy.DotNetMethodToInvokeOnClick = this.DotNetMethodToInvokeOnClick;
            copy.ColorOnClick = this.ColorOnClick;
            copy.SpriteObjType = this.SpriteObjType;
            copy.Vertices = this.Vertices.Select(v => v).ToList();

            return copy;
        }

        public static Dictionary<string, SpriteObject> GetSprites(SceneDescription SceneDesc)
        {
            var Sprites = new Dictionary<string, SpriteObject>();
            foreach (var kvp in SceneDesc.Systems)
            {
                var SystemGuid = kvp.Key;
                var System = kvp.Value;

                int UsedIconVertices = 0;
                if (SceneDesc.DrawCXSprites && System.HasCX)
                {
                    var key = Guid.NewGuid().ToString();
                    var newSprite = new SpriteObject();
                    newSprite.SpriteObjType = (int)SpriteObjectType.CommodityExchange;
                    newSprite.Vertices.AddRange(System.IconVertices[UsedIconVertices]);
                    newSprite.SystemNaturalId = System.NaturalId;
                    newSprite.Color = 0xB88218;
                    newSprite.ColorOnHover = 0xF4A401;
                    newSprite.AddressableId = SystemGuid;
                    newSprite.DotNetMethodToInvokeOnHover = "OnHover";
                    newSprite.SpriteNaturalIdentifiers.Add(System.CXNaturalId);
                    Sprites.Add(key, newSprite);
                    UsedIconVertices++;
                }

                if (SceneDesc.DrawSHYSprites && System.HasShipyard)
                {
                    var key = Guid.NewGuid().ToString();
                    var newSprite = new SpriteObject();
                    newSprite.SpriteObjType = (int)SpriteObjectType.Shipyard;
                    newSprite.Vertices.AddRange(System.IconVertices[UsedIconVertices]);
                    newSprite.SystemNaturalId = System.NaturalId;
                    newSprite.Color = 0xB88218;
                    newSprite.ColorOnHover = 0xF4A401;
                    newSprite.AddressableId = SystemGuid;
                    newSprite.DotNetMethodToInvokeOnHover = "OnHover";
                    newSprite.SpriteNaturalIdentifiers = System.ShipyardLocationNaturalIds;
                    Sprites.Add(key, newSprite);
                    UsedIconVertices++;
                }

                if (SceneDesc.DrawLMSprites && System.HasLocalMarket)
                {
                    var key = Guid.NewGuid().ToString();
                    var newSprite = new SpriteObject();
                    newSprite.SpriteObjType = (int)SpriteObjectType.LocalMarket;
                    newSprite.Vertices.AddRange(System.IconVertices[UsedIconVertices]);
                    newSprite.SystemNaturalId = System.NaturalId;
                    newSprite.Color = 0xB88218;
                    newSprite.ColorOnHover = 0xF4A401;
                    newSprite.AddressableId = SystemGuid;
                    newSprite.DotNetMethodToInvokeOnHover = "OnHover";
                    newSprite.SpriteNaturalIdentifiers = System.LocalMarketLocationNaturalIds;
                    Sprites.Add(key, newSprite);
                    UsedIconVertices++;
                }

                if (SceneDesc.DrawPlayerBaseSprites && System.HasPlayerBase)
                {
                    var key = Guid.NewGuid().ToString();
                    var newSprite = new SpriteObject();
                    newSprite.SpriteObjType = (int)SpriteObjectType.PlayerBase;
                    newSprite.Vertices.AddRange(System.IconVertices[UsedIconVertices]);
                    newSprite.SystemNaturalId = System.NaturalId;
                    newSprite.Color = 0xB88218;
                    newSprite.ColorOnHover = 0xF4A401;
                    newSprite.AddressableId = SystemGuid;
                    newSprite.DotNetMethodToInvokeOnHover = "OnHover";
                    newSprite.SpriteNaturalIdentifiers = System.PlayerStorages.Select(ps => ps.AddressableId).ToList();
                    Sprites.Add(key, newSprite);
                    UsedIconVertices++;
                }

                if (SceneDesc.DrawPlayerStationaryShipSprites && System.HasSystemShips)
                {
                    var key = Guid.NewGuid().ToString();
                    var newSprite = new SpriteObject();
                    newSprite.SpriteObjType = (int)SpriteObjectType.PlayerShipInSystem;
                    newSprite.Vertices.AddRange(System.IconVertices[UsedIconVertices]);
                    newSprite.SystemNaturalId = System.NaturalId;
                    newSprite.Color = 0xB88218;
                    newSprite.ColorOnHover = 0xF4A401;
                    newSprite.AddressableId = SystemGuid;
                    newSprite.DotNetMethodToInvokeOnHover = "OnHover";
                    newSprite.SpriteNaturalIdentifiers = System.PlayerSystemShips.Select(pss => pss.ShipName).ToList();
                    Sprites.Add(key, newSprite);
                    UsedIconVertices++;
                }
            }

            return Sprites;
        }
    }
}
