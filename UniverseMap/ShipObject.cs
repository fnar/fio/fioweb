﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace FIOWeb.UniverseMap
{
    public class ShipFlightSegment
    {
        [JsonPropertyName("departureTimeEpochMs")]
        public long DepartureTimeEpochMs { get; set; }

        [JsonPropertyName("arrivalTimeEpochMs")]
        public long ArrivalTimeEpochMs { get; set; }

        [JsonPropertyName("origin")]
        public string Origin { get; set; }

        [JsonPropertyName("destination")]
        public string Destination { get; set; }
    }

    public class ShipFlight
    {
        [JsonPropertyName("flightId")]
        public string FlightId { get; set; }

        [JsonPropertyName("departureTimeEpochMs")]
        public long DepartureTimeEpochMs { get; set; }

        [JsonPropertyName("arrivalTimeEpochMs")]
        public long ArrivalTimeEpochMs { get; set; }

        [JsonPropertyName("segments")]
        public List<ShipFlightSegment> Segments { get; set; } = new List<ShipFlightSegment>();

        [JsonPropertyName("activeFlightSegment")]
        public ShipFlightSegment ActiveFlightSegment { get; set; }
    }

    public class ShipObject : SceneObjectBase
    {
        [JsonPropertyName("userName")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string UserName { get; set; }

        [JsonPropertyName("shipRegistration")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string ShipRegistration { get; set; }

        [JsonPropertyName("shipName")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string ShipName { get; set; }

        [JsonPropertyName("vertices")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<double> Vertices { get; set; } = null;

        public ShipObject GetStrippedCopy()
        {
            ShipObject copy = new ShipObject();
            copy.IsVisible = this.IsVisible;
            copy.Color = this.Color;
            copy.DotNetMethodToInvokeOnHover = this.DotNetMethodToInvokeOnHover;
            copy.ColorOnHover = this.ColorOnHover;
            copy.DotNetMethodToInvokeOnClick = this.DotNetMethodToInvokeOnClick;
            copy.ColorOnClick = this.ColorOnClick;
            copy.Vertices = this.Vertices;

            return copy;
        }
    }
}
