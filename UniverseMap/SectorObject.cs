﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class SectorObject : SceneObjectBase
    {
        [JsonPropertyName("naturalId")]
        public string NaturalId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        // 6 vertices, 3 coordinates per vertex
        [JsonPropertyName("vertices")]
        public List<double> Vertices { get; set; } = new List<double>(18);

        [JsonPropertyName("centerVertices")]
        public List<double> CenterVertices { get; set; } = new List<double>(3);

        public SectorObject GetStrippedCopy()
        {
            SectorObject copy = new SectorObject();
            copy.IsVisible = this.IsVisible;
            copy.Color = this.Color;
            copy.DotNetMethodToInvokeOnHover = this.DotNetMethodToInvokeOnHover;
            copy.ColorOnHover = this.ColorOnHover;
            copy.DotNetMethodToInvokeOnClick = this.DotNetMethodToInvokeOnClick;
            copy.ColorOnClick = this.ColorOnClick;
            copy.NaturalId = this.NaturalId;
            copy.Name = this.Name;
            copy.Vertices = this.Vertices.Select(v => v).ToList();
            copy.CenterVertices = this.CenterVertices.Select(cv => cv).ToList();

            return copy;
        }

        public static Dictionary<string, SectorObject> GetSectors(List<WorldSector> WorldSectors)
        {
            var Sectors = new Dictionary<string, SectorObject>();

            foreach (var sector in WorldSectors)
            {
                var key = Guid.NewGuid().ToString();
                var s = new SectorObject();
                s.Name = sector.Name;

                // Grab all the edge vertices by only grabbing the ones that are in the SubSector's Vertices twice
                var edgeVertices = sector.SubSectors
                    .SelectMany(ss => ss.Vertices)
                    .GroupBy(v => v)
                    .Select(g => new
                    {
                        Vertex = g.Key,
                        Count = g.Count()
                    })
                    .Where(e => e.Count == 2)
                    .Select(e => e.Vertex)
                    .ToList();

                // Grab the center of the hexagon by averaging the edge vertices
                var center = new SubSectorVertex(
                    edgeVertices.Average(ev => ev.X),
                    edgeVertices.Average(ev => ev.Y),
                    edgeVertices.Average(ev => ev.Z));

                s.CenterVertices.Add(center.X);
                s.CenterVertices.Add(center.Y);
                s.CenterVertices.Add(center.Z);

                // All hexagons vertices are 100 units away from the center, so start by going "east" (+X)
                // Instead of 100, do 90 to match what the game does
                var vector = new SubSectorVertex(90.0, 0.0, 0.0);

                for (int i = 0; i < 6; ++i)
                {
                    var newVertex = center + vector;
                    s.Vertices.Add(newVertex.X);
                    s.Vertices.Add(newVertex.Y);
                    s.Vertices.Add(newVertex.Z);

                    // Move the vector 60 degrees downward
                    const double degrees = -60.0;
                    double theta = degrees * Math.PI / 180.0;
                    var cs = Math.Cos(theta);
                    var sn = Math.Sin(theta);

                    var newVector = new SubSectorVertex();
                    newVector.X = vector.X * cs - vector.Y * sn;
                    newVector.Y = vector.X * sn + vector.Y * cs;

                    vector = newVector;
                }

                s.Color = 0x333333;
                s.ColorOnHover = 0x555555;

                Sectors.Add(key, s);
            }

            return Sectors;
        }
    }
}
