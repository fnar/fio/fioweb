using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class SceneDescription
    {
        // Need to specify a key to get this in IndexDB
        [JsonPropertyName("dummyKey")]
        public string DummyKey { get; set; } = "DummyKey";

        [JsonPropertyName("drawSectors")]
        public bool DrawSectors { get; set; } = true;
        [JsonPropertyName("sectors")]
        public Dictionary<string, SectorObject> Sectors { get; set; } = new Dictionary<string, SectorObject>();

        [JsonPropertyName("drawSystems")]
        public bool DrawSystems { get; set; } = true;
        [JsonPropertyName("systems")]
        public Dictionary<string, SystemObject> Systems { get; set; } = new Dictionary<string, SystemObject>();
        [JsonIgnore]
        public Dictionary<string, string> SystemNaturalIdToSystemGuids
        {
            get
            {
                return Systems.ToDictionary(s => s.Value.NaturalId, s => s.Key);
            }
        }

        [JsonPropertyName("drawFTLConnections")]
        public bool DrawFTLConnections { get; set; } = true;
        [JsonPropertyName("ftlConnections")]
        public Dictionary<string, FTLConnectionObject> FTLConnections { get; set; } = new Dictionary<string, FTLConnectionObject>();

        [JsonPropertyName("drawCXSprites")]
        public bool DrawCXSprites { get; set; } = true;
        [JsonPropertyName("drawSHYSprites")]
        public bool DrawSHYSprites { get; set; } = true;
        [JsonPropertyName("drawLMSprites")]
        public bool DrawLMSprites { get; set; } = false;
        [JsonPropertyName("drawPlayerBaseSprites")]
        public bool DrawPlayerBaseSprites { get; set; } = false;
        [JsonPropertyName("drawPlayerStationaryShipSprites")]
        public bool DrawPlayerStationaryShipSprites { get; set; } = false;

        [JsonPropertyName("drawSprites")]
        public bool DrawSprites { get; set; } = true;
        [JsonPropertyName("sprites")]
        public Dictionary<string, SpriteObject> Sprites { get; set; } = new Dictionary<string, SpriteObject>();

        [JsonPropertyName("drawInFlightShips")]
        public bool DrawInFlightShips { get; set; } = false;
        [JsonPropertyName("inFlightShips")]
        public Dictionary<string, ShipObject> InFlightShips { get; set; } = new Dictionary<string, ShipObject>();

        public SceneDescription GetSceneDescriptionForInterop()
        {
            var interopSceneDesc = (SceneDescription)this.MemberwiseClone();
            interopSceneDesc.Sectors = new Dictionary<string, SectorObject>();
            foreach (var kvp in Sectors)
            {
                interopSceneDesc.Sectors[kvp.Key] = kvp.Value.GetStrippedCopy();
            }
            interopSceneDesc.Systems = new Dictionary<string, SystemObject>();
            foreach (var kvp in Systems)
            {
                interopSceneDesc.Systems[kvp.Key] = kvp.Value.GetStrippedCopy();
            }
            interopSceneDesc.FTLConnections = new Dictionary<string, FTLConnectionObject>();
            foreach (var kvp in FTLConnections)
            {
                interopSceneDesc.FTLConnections[kvp.Key] = kvp.Value.GetStrippedCopy();
            }
            interopSceneDesc.Sprites = new Dictionary<string, SpriteObject>();
            foreach (var kvp in Sprites)
            {
                interopSceneDesc.Sprites[kvp.Key] = kvp.Value.GetStrippedCopy();
            }
            interopSceneDesc.InFlightShips = new Dictionary<string, ShipObject>();
            foreach (var kvp in InFlightShips)
            {
                interopSceneDesc.InFlightShips[kvp.Key] = kvp.Value.GetStrippedCopy();
            }

            return interopSceneDesc;
        }
    }
}
