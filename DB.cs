﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using BlazorDB;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

using FIOWeb.JsonPayloads;

namespace FIOWeb
{
    public class DB
    {
        private IBlazorDbFactory _factory = null;

        public DB(IBlazorDbFactory factory)
        {
            _factory = factory;
        }

        private Dictionary<string, ExpiryStore> _ExpiryMemoryCache = new Dictionary<string, ExpiryStore>();
        private void SetExpiryMemoryCache(List<ExpiryStore> AllExpiryStores)
        {
            AllExpiryStores.ForEach(f =>
            {
                if (_ExpiryMemoryCache.ContainsKey(f.Page))
                {
                    _ExpiryMemoryCache[f.Page] = f;
                }
                else
                {
                    _ExpiryMemoryCache.Add(f.Page, f);
                }
            });
        }

        private Dictionary<string, string> _DataMemoryCache = new Dictionary<string, string>();
        private void SetDataMemoryCache<T>(string Page, T Data)
        {
            var stringData = JsonConvert.SerializeObject(Data);
            if (_DataMemoryCache.ContainsKey(Page))
            {
                _DataMemoryCache[Page] = stringData;
            }
            else
            {
                _DataMemoryCache.Add(Page, stringData);
            }
        }
        private T GetDataFromMemoryCache<T>(string Page) where T : class
        {
            if (_DataMemoryCache.ContainsKey(Page))
            {
                return JsonConvert.DeserializeObject<T>(_DataMemoryCache[Page]);
            }

            return null;
        }

        private IndexedDbManager _dbManager = null;
        private async Task<IndexedDbManager> GetDBManager()
        {
            if (_dbManager == null)
            {
                _dbManager = await _factory.GetDbManager("FIOWeb");
                await _dbManager.OpenDb();
            }

            return _dbManager;
        }

        private async Task<List<T>> InternalGetData<T>(string StoreName)
        {
            var CachedData = GetDataFromMemoryCache<List<T>>(StoreName);
            if (CachedData == null)
            {
                var dbManager = await GetDBManager();
                var CachedDataIList = await dbManager.ToArray<T>(StoreName);
                if (CachedDataIList != null)
                {
                    CachedData = CachedDataIList.ToList();
                }
                else
                {
                    CachedData = new List<T>();
                }
            }

            return CachedData;
        }

        private async Task InternalSetExpiry(string StoreName, long NewEpochMsExpiry)
        {
            var allExpiryStores = await InternalGetData<ExpiryStore>("ExpiryStore");
            var expiryStore = allExpiryStores.FirstOrDefault(es => es.Page == StoreName);
            if (expiryStore == null)
            {
                expiryStore = new ExpiryStore { Page = StoreName, EpochMsExpiry = NewEpochMsExpiry, EpochMsStoreTime = Utils.GetCurrentEpochMs() };
                allExpiryStores.Add(expiryStore);
            }
            else
            {
                expiryStore.EpochMsExpiry = NewEpochMsExpiry;
                expiryStore.EpochMsStoreTime = Utils.GetCurrentEpochMs();
            }

            var dbManager = await GetDBManager();
            await dbManager.ClearTableAsync("ExpiryStore");
            await dbManager.BulkAddRecordAsync("ExpiryStore", allExpiryStores);
            SetExpiryMemoryCache(allExpiryStores);
        }

        private async Task InternalSetData<T>(string StoreName, List<T> Data, long EpochMsExpiry)
        {
            if (Data == null)
            {
                return;
            }

            System.Diagnostics.Debug.Assert(StoreName != "ExpiryStore");
            if (StoreName != "ExpiryStore")
            {
                await InternalSetExpiry(StoreName, EpochMsExpiry);

                var dbManager = await GetDBManager();
                await dbManager.ClearTableAsync(StoreName);
                await dbManager.BulkAddRecordAsync(StoreName, Data);
                
                SetDataMemoryCache(StoreName, Data);
            }
        }

        public async Task<bool> HasStoreExpired(string StoreName)
        {
            if (StoreName == "ExpiryStore")
            {
                return false;
            }

            if (_ExpiryMemoryCache.ContainsKey(StoreName))
            {
                return Utils.GetCurrentEpochMs() > _ExpiryMemoryCache[StoreName].EpochMsExpiry;
            }
            else
            {
                var allExpiryStores = await InternalGetData<ExpiryStore>("ExpiryStore");
                if (allExpiryStores != null)
                {
                    var expiryStore = allExpiryStores.FirstOrDefault(es => es.Page == StoreName);
                    if (expiryStore != null)
                    {
                        return Utils.GetCurrentEpochMs() > expiryStore.EpochMsExpiry;
                    }
                }
            }

            return true;
        }

        public async Task<long> GetStoreTimestamp(string StoreName)
        {
            if (StoreName == "ExpiryStore")
            {
                return default(long);
            }

            if (_ExpiryMemoryCache.ContainsKey(StoreName))
            {
                return _ExpiryMemoryCache[StoreName].EpochMsStoreTime;
            }
            else
            {
                var allExpiryStores = await InternalGetData<ExpiryStore>("ExpiryStore");
                if (allExpiryStores != null)
                {
                    var expiryStore = allExpiryStores.FirstOrDefault(es => es.Page == StoreName);
                    if (expiryStore != null)
                    {
                        return expiryStore.EpochMsStoreTime;
                    }
                }
            }

            return default(long);
        }

        public async Task SetData<T>(string Page, List<T> Data, TimeSpan TimeUntilExpiry)
        {
            if (Data != null)
            {
                var EpochMsExpiry = Utils.GetCurrentEpochMs() + (long)TimeUntilExpiry.TotalMilliseconds;
                await InternalSetData<T>(Page, Data, EpochMsExpiry);
            }
        }

        public async Task<List<T>> GetData<T>(string Page, Func<Task<List<T>>> Get, TimeSpan TimeUntilExpiry, bool AllowExpired = false) where T : class
        {
            List<T> Data = null;
            if (AllowExpired || !await HasStoreExpired(Page))
            {
                Data = await InternalGetData<T>(Page);
            }

            if (Data == null || Data.Count == 0)
            {
                Data = await Get();
                await SetData<T>(Page, Data, TimeUntilExpiry);
            }

            return Data;
        }
    }

    internal class ExpiryStore
    {
        public string Page { get; set; }
        public long EpochMsStoreTime { get; set; }
        public long EpochMsExpiry { get; set; }
    }

    public static class DBRegister
    {
        private static List<StoreSchema> _StoreSchemas = new List<StoreSchema>();

        static DBRegister()
        {
            Register("ExpiryStore", "Page");
        }

        public static void Register(string Page, string Index)
        {
            _StoreSchemas.Add(
                new StoreSchema()
                {
                    Name = Page,
                    PrimaryKey = "Id",
                    PrimaryKeyAuto = true,
                    Indexes = new List<string>
                    {
                        Index
                    }
                });
        }

        private const int DBVersion = 5;
        public static IServiceCollection AddDB(this IServiceCollection SC)
        {
            return SC.AddBlazorDB(options =>
            {
                options.Name = "FIOWeb";
                options.Version = DBVersion;
                options.StoreSchemas = _StoreSchemas;
            });
        }
    }

    public static class DBGlobals
    {
        private static TimeSpan MaterialExpiryTime = new TimeSpan(days: 1, hours: 0, minutes: 0, seconds: 0);
        private static TimeSpan ComexExchangesExpiryTime = new TimeSpan(days: 1, hours: 0, minutes: 0, seconds: 0);
        private static TimeSpan WorldMapExpiryTime = new TimeSpan(days: 5, hours: 0, minutes: 0, seconds: 0);
        private static TimeSpan PlanetSearchExpiryTime = new TimeSpan(hours: 1, minutes: 0, seconds: 0);

        public static async Task<List<MaterialPayload>> GetMinerals(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Minerals", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/minerals");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetOres(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Ores", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/ores");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetGases(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Gases", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/gases");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetLiquids(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Liquids", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/liquids");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetAllMaterials(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-All", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/allmaterials");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetBasicConsumables(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Basic", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/consumables (basic)");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<MaterialPayload>> GetLuxuryConsumables(DB Db)
        {
            return await Db.GetData<MaterialPayload>("Materials-Luxury", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/material/category/consumables (luxury)");
                return await req.GetResponseAsync<List<MaterialPayload>>();
            }, MaterialExpiryTime);
        }

        public static async Task<List<ExchangePayload>> GetComexExchanges(DB Db)
        {
            return await Db.GetData<ExchangePayload>("ComexExchanges", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/global/comexexchanges");
                return await req.GetResponseAsync<List<ExchangePayload>>();
            }, ComexExchangesExpiryTime);
        }

        public static async Task<List<SystemStarPayload>> GetSystemStars(DB Db)
        {
            return await Db.GetData<SystemStarPayload>("SystemStars", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/systemstars");
                return await req.GetResponseAsync<List<SystemStarPayload>>();
            }, WorldMapExpiryTime);
        }

        public static async Task<List<WorldSector>> GetWorldSectors(DB Db)
        {
            return await Db.GetData<WorldSector>("WorldSectors", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/systemstars/worldsectors");
                return await req.GetResponseAsync<List<WorldSector>>();
            }, WorldMapExpiryTime);
        }

        public static async Task<List<RainSystemLink>> GetSystemLinks(DB Db)
        {
            return await Db.GetData<RainSystemLink>("SystemLinks", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/rain/systemlinks");
                return await req.GetResponseAsync<List<RainSystemLink>>();
            }, WorldMapExpiryTime);
        }

        public static async Task<List<Models.PlanetSearchModel>> GetPlanetSearchModels(DB Db)
        {
            return await Db.GetData<Models.PlanetSearchModel>("PlanetSearch", async () =>
            {
                var Minerals = await DBGlobals.GetMinerals(Db);
                var Ores = await DBGlobals.GetOres(Db);
                var Liquids = await DBGlobals.GetLiquids(Db);
                var Gases = await DBGlobals.GetGases(Db);
                var AllPlanetMaterials = Minerals.Union(Ores.Union(Liquids.Union(Gases))).ToList();

                var ComexExchanges = await DBGlobals.GetComexExchanges(Db);
                var ExchangeLocationNames = ComexExchanges.Select(e => e.LocationName).ToList();
                return await Models.PlanetSearchModel.GetAllPlanets(AllPlanetMaterials, ExchangeLocationNames);
            }, PlanetSearchExpiryTime);
        }

        public static async Task<List<StationPayload>> GetStations(DB Db)
        {
            return await Db.GetData<StationPayload>("Stations", async () =>
            {
                var req = new Web.Request(HttpMethod.Get, "/exchange/station");
                return await req.GetResponseAsync<List<StationPayload>>();
            }, WorldMapExpiryTime);
        }
    }
}
