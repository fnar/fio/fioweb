using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FIOWeb.Models
{
    public class ShipmentGPSModel
    {
        public string ContractLocalId { get; set; }
        public string PartnerCompanyCode { get; set; }
        public string ShipmentItemId { get; set; }
        public string PartnerUserName { get; set; }

        public bool IsTracked { get; set; } = false;
        public string Location { get; set; }
        public FLIGHTSFlight Flight { get; set; }

        public string LocationDisplay
        {
            get
            {
                if (IsTracked)
                {
                    if (Location != null)
                    {
                        return Location;
                    }
                    else
                    {
                        return $"{Flight.Destination} @ {Flight.ArrivalTimeEpochMs.FromUnixTime()}";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public DateTime LastUpdateTime { get; set; }
    }

    public class FLIGHTSFlight
    {
        public string FlightId { get; set; }
        public string ShipId { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public List<FLIGHTSFlightSegment> Segments { get; set; } = new List<FLIGHTSFlightSegment>();

        public int CurrentSegmentIndex { get; set; }

        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }

        public bool IsAborted { get; set; }
    }

    public class FLIGHTSFlightSegment
    {
        public string Type { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();
    }

    public class OriginLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }

    public class DestinationLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }
}