using System;

namespace FIOWeb.Models
{
    public class PermissionAllowance
    {
        public string UserName { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }

        public bool Matches(PermissionAllowance other)
        {
            bool bMatches = true;
            if (other.FlightData && !FlightData) bMatches = false;
            if (other.BuildingData && !BuildingData) bMatches = false;
            if (other.StorageData && !StorageData) bMatches = false;
            if (other.ProductionData && !ProductionData) bMatches = false;
            if (other.WorkforceData && !WorkforceData) bMatches = false;
            if (other.ExpertsData && !ExpertsData) bMatches = false;

            return bMatches;
        }
    }
}