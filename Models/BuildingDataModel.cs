using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FIOWeb.Models
{
    public enum CheckedState
    {
        Unchecked,
        Indeterminate,
        Checked
    }

    public class BuildingDataModel : ICloneable
    {
        public BuildingDataModel(BuildingDataModel parent = null)
        {
            Parent = parent;
        }

        public CheckedState CheckState { get; private set; }

        private BuildingDataModel ParentMost
        {
            get
            {
                BuildingDataModel p = this;
                while(p.Parent != null )
                {
                    p = p.Parent;
                }

                return p;
            }
        }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; } = "";
        public string PlanetName { get; set; } = "";

        public string PlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
            set
            {
                // Intentionally left empty
            }
        }

        public bool IsCoreBaseBuilding { get; set; } = false;
        public string BuildingTicker { get; set; } = "";
        public float? Condition { get; set; }

        public string MaterialTicker { get; set; } = "";
        public int? MaterialAmount { get; set; }

        public BuildingDataModel Parent { get; set; } = null;
        public List<BuildingDataModel> Children { get; set; } = new List<BuildingDataModel>();

        public object Clone()
        {
            BuildingDataModel model = new BuildingDataModel();

            model.CheckState = CheckState;
            model.PlanetId = PlanetId;
            model.PlanetNaturalId = PlanetNaturalId;
            model.PlanetName = PlanetName;

            model.IsCoreBaseBuilding = IsCoreBaseBuilding;
            model.BuildingTicker = BuildingTicker;
            model.Condition = Condition;
            model.MaterialTicker = MaterialTicker;
            model.MaterialAmount = MaterialAmount;
            
            foreach (var child in Children)
            {
                var childBDM = (BuildingDataModel)child.Clone();
                childBDM.Parent = model;
                model.Children.Add(childBDM);
            }
            
            return model;
        }

        public void Check()
        {
            switch (CheckState)
            {
                case CheckedState.Unchecked:
                case CheckedState.Indeterminate:
                    // Mark ourselves and all children as checked
                    CheckState = CheckedState.Checked;

                    foreach (var child in Children)
                    {
                        child.CheckState = CheckedState.Checked;
                        foreach( var innerChild in child.Children)
                        {
                            innerChild.CheckState = CheckedState.Checked;
                        }
                    }
                    break;

                case CheckedState.Checked:
                    // Mark ourselves and all children as unchecked
                    CheckState = CheckedState.Unchecked;
                    foreach (var child in Children)
                    {
                        child.CheckState = CheckedState.Unchecked;
                        foreach (var innerChild in child.Children)
                        {
                            innerChild.CheckState = CheckedState.Unchecked;
                        }
                    }
                    break;
            }

            // Recalc checked values upward
            RecalcCheckState(Parent);
        }

        private void RecalcCheckState(BuildingDataModel model)
        {
            if ( model == null)
            {
                return;
            }

            var ChildrenCount = model.Children.Count;
            var UncheckedCount = model.Children.Count(c => c.CheckState == CheckedState.Unchecked);
            var CheckedCount = model.Children.Count(c => c.CheckState == CheckedState.Checked);
            if (UncheckedCount == ChildrenCount)
            {
                // We're unchecked
                model.CheckState = CheckedState.Unchecked;
            }
            else if (CheckedCount == ChildrenCount)
            {
                // We're checked
                model.CheckState = CheckedState.Checked;
            }
            else
            {
                // We're indeterminate, recurse
                model.CheckState = CheckedState.Indeterminate;
            }

            RecalcCheckState(model.Parent);
        }

        public static async Task<List<BuildingDataModel>> GetBuildingDataModels(string UserName, string AuthToken)
        {
            List<BuildingDataModel> results = new List<BuildingDataModel>();

            var buildingsRequest = new Web.Request(HttpMethod.Get, "/building/corebasebuildings");
            var buildingResponse = await buildingsRequest.GetResponseAsync<List<JsonPayloads.BuildingPayload>>();
            if (buildingResponse != null)
            {
                List<string> coreBuildings = buildingResponse.Select(b => b.Ticker).ToList();

                var sitesRequest = new Web.Request(HttpMethod.Get, $"/sites/{UserName}", AuthToken);
                var sites = await sitesRequest.GetResponseAsync<List<JsonPayloads.SitesPayload>>();
                if (sites != null)
                {
                    foreach (var site in sites)
                    {
                        BuildingDataModel siteModel = new BuildingDataModel();
                        siteModel.PlanetId = site.PlanetId;
                        siteModel.PlanetNaturalId = site.PlanetIdentifier;
                        siteModel.PlanetName = site.PlanetName;

                        foreach( var building in site.Buildings )
                        {
                            BuildingDataModel buildingModel = new BuildingDataModel(siteModel);
                            buildingModel.IsCoreBaseBuilding = coreBuildings.Contains(building.BuildingTicker);

                            buildingModel.BuildingTicker = building.BuildingTicker;
                            buildingModel.Condition = building.Condition;

                            foreach( var repair in building.RepairMaterials)
                            {
                                BuildingDataModel repairItemModel = new BuildingDataModel(buildingModel);
                                repairItemModel.MaterialTicker = repair.MaterialTicker;
                                repairItemModel.MaterialAmount = repair.MaterialAmount;

                                buildingModel.Children.Add(repairItemModel);
                            }

                            if ( buildingModel.Children.Count > 0 || buildingModel.IsCoreBaseBuilding )
                            {
                                siteModel.Children.Add(buildingModel);
                            }
                        }

                        if (siteModel.Children.Count > 0)
                        {
                            results.Add(siteModel);
                        }
                    }
                }
            }

            return results;
        }
    }

    public class ShoppingCartModel
    {
        public string Ticker { get; set; }
        public int Amount { get; set; }
    }
}
