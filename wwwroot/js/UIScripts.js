/**
 * @author DaDarkWizard / https://github.com/DaDarkWizard
 */


function resetBody() {
    // Clear any styles on the document body.
    // This is necessary because of a bug in the AntDesign package.
    // This bug causes the scroll bar to disappear after opening a Drawer.
    // Exists up to version 0.15.0
    document.body.style = "";
}