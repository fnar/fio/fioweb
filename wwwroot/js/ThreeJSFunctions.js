import * as THREE from '../js/three.module.js';
import { OrbitControls } from '../js/OrbitControls.js';

var container, clock, controls;
var camera, scene, renderer, mixer;

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

function animate() {
    controls.update(1);
    requestAnimationFrame(animate);
    render();
}

function render() {
    var delta = clock.getDelta();
    if (mixer !== undefined) {
        mixer.update(delta);
    }

    renderer.render(scene, camera);
}

const SPRITE_CX = 0;
const PLAYER_SHIP_IN_SYSTEM = 1;
const SPRITE_SHIP_TRAVELING = 2;
const SPRITE_SHIPYARD = 3;
const SPRITE_INVENTORY = 4;
const SPRITE_BASE = 5;
const SPRITE_LOCAL_MARKET = 6;

const textureLoader = new THREE.TextureLoader();
const spriteLocations = [
    "../img/icons/commodity_exchange.png",
    "../img/icons/ship_in_transit.png",
    "../img/icons/ship_in_transit.png",
    "../img/icons/shipyard.png",
    "../img/icons/inventory.png",
    "../img/icons/base.png",
    "../img/icons/local_market.png"
]

var spriteMaps = [];
var spriteMaterials = [];
var sprites = [];
for (var i = 0; i < spriteLocations.length; ++i) {
    spriteMaps.push(textureLoader.load(spriteLocations[i]));
    spriteMaterials.push(new THREE.SpriteMaterial({ map: spriteMaps[i], sizeAttenuation: false, transparent: true, depthWrite: false, depthTest: false }));
    var sprite = new THREE.Sprite(spriteMaterials[i]);
    sprite.scale.set(0.05, 0.05, 0.05);
    sprites.push(sprite);
}

function getSprite(spriteIndex) {
    if (spriteIndex < 0 || spriteIndex > SPRITE_LOCAL_MARKET)
        return null;

    var sprite = sprites[spriteIndex].clone();
    sprite.material = sprite.material.clone();
    return sprite;
}

const fontLoader = new THREE.FontLoader();
let font = undefined;
fontLoader.load("../fonts/Excalibur.typeface.json", function (res) {
    font = res;
});

const textmaterial = new THREE.MeshPhongMaterial({ color: 0xffffff, flatShading: true });

var SectorsMap = undefined;
var SystemsMap = undefined;
var SpritesMap = undefined;
var FTLConnectionsMap = undefined;
var InFlightShipsMap = undefined;

const GUID_INDEX = 0;
const OBJECT_INDEX = 1;

function loadScene(sceneDescription) {

    container = document.getElementById('threejscontainer');
    if (!container) {
        return;
    }

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x222222);

    camera = new THREE.PerspectiveCamera(70, container.clientWidth / container.clientHeight, 10, 100000);
    camera.up.set(0, 0, 1);
    camera.position.set(0, -500, 5000);
    camera.rotation.set(90, 90, 0);
    camera.zoom = 1;

    
    clock = new THREE.Clock();

    const ambientLight = new THREE.AmbientLight(0xB0B0B0);
    scene.add(ambientLight);
    
    const directionalLight = new THREE.DirectionalLight(0x404040, 0.5);
    directionalLight.position.set(20, 100, 100);
    scene.add(directionalLight);

    if (sceneDescription.drawSprites) {
        SpritesMap = new Map(Object.entries(sceneDescription.sprites));
        for (const [key, value] of SpritesMap) {
            var spriteDescription = value;
            var sprite = getSprite(spriteDescription.spriteObjType);
            sprite.visible = spriteDescription.isVisible;
            sprite.position.set(spriteDescription.vertices[0], spriteDescription.vertices[1], spriteDescription.vertices[2]);
            sprite.Attributes = [key, value];
            sprite.material.color.set(spriteDescription.color);
            scene.add(sprite);
        }
    }

    if (sceneDescription.drawInFlightShips) {
        InFlightShipsMap = new Map(Object.entries(sceneDescription.inFlightShips));
        for (const [key, value] of InFlightShipsMap) {
            var shipDescription = value;
            var ship = getSprite(1);
            sprite.visible = shipDescription.isVisible;
            ship.position.set(shipDescription.vertices[0], shipDescription.vertices[1], shipDescription.vertices[2]);
            ship.Attributes = [key, value];
            ship.material.color.set(shipDescription.color);
            scene.add(ship);
        }
    }

    if (sceneDescription.drawSectors) {
        SectorsMap = new Map(Object.entries(sceneDescription.sectors));
        for (const [key, value] of SectorsMap) {
            var Sector = value;
            var sectorMaterial = new THREE.MeshBasicMaterial({ color: Sector.color });
            sectorMaterial.side = THREE.DoubleSide;
            sectorMaterial.transparent = true;
            sectorMaterial.opacity = 0.5;

            const sectorGeo = new THREE.Geometry();
            for (var i = 0; i < 6; ++i) {
                sectorGeo.vertices.push(new THREE.Vector3(Sector.vertices[i * 3 + 0], Sector.vertices[i * 3 + 1], Sector.vertices[i * 3 + 2]));
            }
            sectorGeo.vertices.push(new THREE.Vector3(Sector.centerVertices[0], Sector.centerVertices[1], Sector.centerVertices[2]));

            const centerIndex = 6;
            sectorGeo.faces.push(new THREE.Face3(0, 1, centerIndex));
            sectorGeo.faces.push(new THREE.Face3(1, 2, centerIndex));
            sectorGeo.faces.push(new THREE.Face3(2, 3, centerIndex));
            sectorGeo.faces.push(new THREE.Face3(3, 4, centerIndex));
            sectorGeo.faces.push(new THREE.Face3(4, 5, centerIndex));
            sectorGeo.faces.push(new THREE.Face3(5, 0, centerIndex));

            sectorGeo.computeFaceNormals();
            const sectorMesh = new THREE.Mesh(sectorGeo, sectorMaterial);
            sectorMesh.visible = Sector.isVisible;
            sectorMesh.Attributes = [key, value];
            scene.add(sectorMesh);

            let textGeo = new THREE.TextGeometry(Sector.name, {
                font: font,
                size: 50,
                height: 0.01,
            });

            textGeo.computeBoundingBox();

            const centerOffsetX = -0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);
            const centerOffsetY = -0.5 * (textGeo.boundingBox.max.y - textGeo.boundingBox.min.y);

            let textMesh = new THREE.Mesh(textGeo, textmaterial)
            textMesh.position.x = Sector.centerVertices[0] + centerOffsetX;
            textMesh.position.y = Sector.centerVertices[1] + centerOffsetY;
            textMesh.position.z = Sector.centerVertices[2];
            textMesh.rotation.x = 0;
            textMesh.rotation.y = Math.PI * 2;
            textMesh.visible = Sector.isVisible;  // How to handle toggling this if necessary?
            scene.add(textMesh);
        }
    }

    if (sceneDescription.drawSystems) {
        SystemsMap = new Map(Object.entries(sceneDescription.systems));
        for (const [key, value] of SystemsMap) {
            var System = value;
            var geometry = new THREE.SphereGeometry(System.starRadius * 2.0, 32, 32);
            var material = new THREE.MeshBasicMaterial({ color: System.color });
            var sphere = new THREE.Mesh(geometry, material);
            sphere.position.set(System.vertices[0], System.vertices[1], System.vertices[2]);
            sphere.visible = System.isVisible;
            sphere.Attributes = [key, value];
            scene.add(sphere);
        }
    }

    if (sceneDescription.drawFTLConnections) {
        FTLConnectionsMap = new Map(Object.entries(sceneDescription.ftlConnections));
        for (const [key, value] of FTLConnectionsMap) {
            var FTLConnection = value;
            var material = new THREE.LineBasicMaterial({ color: FTLConnection.color });
            material.linewidth = 0.25;
            material.transparent = true;
            material.opacity = 1.0;

            var points = []
            points.push(new THREE.Vector3(FTLConnection.startVertices[0], FTLConnection.startVertices[1], FTLConnection.startVertices[2]));
            points.push(new THREE.Vector3(FTLConnection.endVertices[0], FTLConnection.endVertices[1], FTLConnection.endVertices[2]));

            var geometry = new THREE.BufferGeometry().setFromPoints(points);
            var line = new THREE.Line(geometry, material);
            line.visible = FTLConnection.isVisible;
            line.Attributes = [key, value];
            scene.add(line);
        }
    }

    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(container.clientWidth, container.clientHeight);


    while (container.lastElementChild) {
        container.removeChild(container.lastElementChild);
    }

    container.appendChild(renderer.domElement);

    //controls = new THREE.MapControls(camera, renderer.domElement);
    controls = new OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.15;
    controls.minDistance = 100;
    controls.maxDistance = 55000;
    controls.maxPolarAngle = 7.0 / 8.0 * Math.PI / 2.0;
    controls.mouseButtons.LEFT = THREE.MOUSE.PAN;
    controls.mouseButtons.RIGHT = THREE.MOUSE.ROTATE;
    controls.touches.ONE = THREE.TOUCH.PAN;
    controls.touches.TWO = THREE.TOUCH.DOLLY_ROTATE;

    //controls.screenSpacePanning = true;
    //controls.minDistance = 200;
    //controls.maxDistance = 1500;
    //controls.target.set(0, 2, 0);
    controls.update();

    animate();

    renderer.domElement.addEventListener('mousemove', onMouseOver, false);
    renderer.domElement.addEventListener('mousedown', onMouseDown, false);
    renderer.domElement.addEventListener('mouseup', onMouseUp, false);
}

function updateObjects(objs) {
    console.log("updateObjects invoked");
}

var previousMouseOvers = [];

function onMouseOver(event) {
    mouse.x = (event.offsetX / container.clientWidth) * 2 - 1;
    mouse.y = - (event.offsetY / container.clientWidth) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);

    for (var prevI = 0; prevI < previousMouseOvers.length; ++prevI) {
        var obj = previousMouseOvers[prevI];
        obj.material.color.set(obj.Attributes[OBJECT_INDEX].color);
    }
    previousMouseOvers = [];

    var intersects = raycaster.intersectObjects(scene.children, true);

    var objToInvokeOn = null;

    // if there is one (or more) intersections
    for (var i = 0; i < intersects.length; i++) {   
        var obj = intersects[i].object;
        if (obj.Attributes !== undefined) {
            if (obj.Attributes[OBJECT_INDEX].colorOnHover != 0) {
                obj.material.color.set(obj.Attributes[OBJECT_INDEX].colorOnHover);
                previousMouseOvers.push(obj);
            }

            if (objToInvokeOn === null && obj.Attributes[OBJECT_INDEX].dotNetMethodToInvokeOnHover !== null) {
                objToInvokeOn = obj;
            }
        }
    }

    // Only invoke on the first intersect found
    if (objToInvokeOn !== null) {
        DotNetObjRef.invokeMethodAsync(objToInvokeOn.Attributes[OBJECT_INDEX].dotNetMethodToInvokeOnHover, objToInvokeOn.Attributes[GUID_INDEX]);
    }
}

var previousMouseDowns = [];

function onMouseDown(event) {
    mouse.x = (event.offsetX / container.clientWidth) * 2 - 1;
    mouse.y = - (event.offsetY / container.clientWidth) * 2 + 1;
    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children, true);

    var objToInvokeOn = null;
    
    // if there is one (or more) intersections
    for (var i = 0; i < intersects.length; i++) {
        var obj = intersects[i].object;
        if (obj.Attributes !== undefined) {
            if (obj.Attributes[OBJECT_INDEX].colorOnClick != 0) {
                obj.material.color.set(obj.Attributes[OBJECT_INDEX].colorOnClick);
                previousMouseDowns.push(obj);
            }

            if (objToInvokeOn === null && obj.Attributes[OBJECT_INDEX].dotNetMethodToInvokeOnClick !== null) {
                objToInvokeOn = obj;
            }
        }
    }

    // Only invoke on the first intersect found
    if (objToInvokeOn !== null) {
        DotNetObjRef.invokeMethodAsync(objToInvokeOn.Attributes[OBJECT_INDEX].dotNetMethodToInvokeOnClick, objToInvokeOn.Attributes[GUID_INDEX]);
    }
}

function onMouseUp(event) {
    for (var prevI = 0; prevI < previousMouseDowns.length; ++prevI) {
        var obj = previousMouseDowns[prevI];
        obj.material.color.set(obj.Attributes[OBJECT_INDEX].color);
    }
    previousMouseDowns = [];
}

function onStart() {
    isPlayed = true;
}

function onStop() {
    isPlayed = false;
}

var DotNetObjRef = null;

window.ThreeJSFunctions = {
    load: (sceneDescription) => { loadScene(sceneDescription); },
    update: (objs) => { updateObjects(objs); },
    stop: () => { onStop(); },
    start: () => { onStart(); },
    setMapHandler: (dotNetObjRef) => { DotNetObjRef = dotNetObjRef;},
};

window.onload = loadScene;
