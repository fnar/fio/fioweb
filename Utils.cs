using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Components;

namespace FIOWeb
{
	public static class Utils
	{
		private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		public static DateTime FromUnixTime(this long unixTime)
		{
			return epoch.AddMilliseconds(unixTime);
		}

		public static long ToUnixTime(this DateTime thisTime)
		{
			TimeSpan t = thisTime - epoch;
			return (long)t.TotalMilliseconds;
		}

		public static long GetCurrentEpochMs()
		{
			TimeSpan t = DateTime.UtcNow - epoch;
			return (long)t.TotalMilliseconds;
		}

		public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
		{
			return listToClone.Select(item => (T)item.Clone()).ToList();
		}

		public static string RoundToSignificantDigitsStr(this double value, int numberOfSignificantDigits)
        {
			return value.ToString($"G{numberOfSignificantDigits}");
		}

		public static double RoundToSignificantDigits(this double value, int numberOfSignificantDigits)
		{
			return double.Parse(RoundToSignificantDigitsStr(value, numberOfSignificantDigits));
		}

		public static double Lerp(double start, double end, double percentage)
		{
			return (start + (end - start) * percentage);
		}

		// For disabling "Unreachable code detected"
		public static bool False = false;
	}
}