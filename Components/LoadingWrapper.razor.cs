﻿using Microsoft.AspNetCore.Components;

namespace FIOWeb.Components
{
	public partial class LoadingWrapper
	{
		[Parameter]
		public RenderFragment ChildContent { get; set; } = null!;

        [Parameter]
		public bool RequiresAuth { get; set; } = false;

		[Parameter]
		public bool RequiresAdmin { get; set; } = false;

		[Parameter]
		public int ProgressCount 
		{ 
			get => _ProgressCount; 
			set
            {
				if (_ProgressCount != value)
                {
					_ProgressCount = value;
					ProgressCountChanged.InvokeAsync(value);
				}
            }
		}
		private int _ProgressCount = 0;

		[Parameter]
		public EventCallback<int> ProgressCountChanged { get; set; }

		[Parameter]
		public int TotalProgress 
		{
			get => _TotalProgress;
			set
            {
				if (_TotalProgress != value)
                {
					_TotalProgress = value;
					TotalProgressChanged.InvokeAsync(value);
				}
            }
		}
		private int _TotalProgress = 0;

		[Parameter]
		public EventCallback<int> TotalProgressChanged { get; set; }

		public bool IsLoaded
		{
			get
			{
				return TotalProgress == 0 || ProgressCount == TotalProgress;
			}
		}

		public string ProgressStr
		{
			get
			{
				if (IsLoaded)
				{
					return "100%";
				}
				else
				{
					if (TotalProgress > 1)
                    {
						double value = ((double)ProgressCount / TotalProgress) * 100.0;
						return Utils.RoundToSignificantDigitsStr(value, 2) + "%";
					}
					else
                    {
						return string.Empty;
                    }
				}
			}
		}
	}
}