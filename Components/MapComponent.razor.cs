﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

using Newtonsoft.Json;
using FIOWeb.UniverseMap;

namespace FIOWeb.Components
{
    public partial class MapComponent
    {
        [Parameter]
        public string Width { get; set; } = "500px";

        [Parameter]
        public string Height { get; set; } = "500px";

        [Parameter]
        public EventCallback<UniverseMap.SystemObject> OnSystemHovered { get; set; }
        private UniverseMap.SystemObject SystemHovered { get; set; }

        [Parameter]
        public EventCallback<UniverseMap.SystemObject> OnSystemClicked { get; set; }
        private UniverseMap.SystemObject SystemClicked { get; set; }


        [Parameter]
        public EventCallback<UniverseMap.SpriteObject> OnSpriteHovered { get; set; }
        private UniverseMap.SpriteObject SpriteHovered { get; set; }

        [Parameter]
        public EventCallback<UniverseMap.SpriteObject> OnSpriteClicked { get; set; }
        private UniverseMap.SpriteObject SpriteClicked { get; set; }


        [Parameter]
        public EventCallback<UniverseMap.ShipObject> OnShipHovered { get; set; }
        private UniverseMap.ShipObject ShipHovered { get; set; }

        [Parameter]
        public EventCallback<UniverseMap.ShipObject> OnShipClicked { get; set; }
        private UniverseMap.ShipObject ShipClicked { get; set; }

        [Parameter]
        public EventCallback OnFinishedLoading { get; set; }

        public UniverseMap.SceneDescription SceneDescription { get; set; } = new UniverseMap.SceneDescription();
        private UniverseMap.SceneDescription SceneDescriptionForInterop = null;

        private int CurrentProgress = 0;
        private int TotalProgress = 9;
        private void SetProgress(int Progress)
        {
            CurrentProgress = Progress;
            StateHasChanged();
        }

        private DB Db = null;
        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;
            Db = new DB(DbFactory);

            await Refresh();
        }

        private static TimeSpan MapSceneDescriptionExpiry = new TimeSpan(days: 5, hours: 0, minutes: 0, seconds: 0);
        private async Task Refresh()
        {
            SetProgress(0);
            var SceneDescriptions = await Db.GetData<UniverseMap.SceneDescription>("MapSceneDescription", async () =>
            {
                var SystemStars = await DBGlobals.GetSystemStars(Db);
                SetProgress(1);
                var WorldSectors = await DBGlobals.GetWorldSectors(Db);
                SetProgress(2);
                var SystemLinks = await DBGlobals.GetSystemLinks(Db);
                SetProgress(3);
                var Stations = await DBGlobals.GetStations(Db);
                SetProgress(4);
                var PlanetSearchModels = await DBGlobals.GetPlanetSearchModels(Db);
                SetProgress(5);
                var SceneDescription = new UniverseMap.SceneDescription();
                SceneDescription.Sectors = SectorObject.GetSectors(WorldSectors);
                SetProgress(6);
                SceneDescription.Systems = SystemObject.GetSystems(Stations, SystemStars, PlanetSearchModels);
                SetProgress(7);
                SceneDescription.FTLConnections = FTLConnectionObject.GetFTLConnections(SystemStars, SystemLinks);

                return new List<UniverseMap.SceneDescription> { SceneDescription };
            }, MapSceneDescriptionExpiry);

            SetProgress(8);

            SceneDescription = SceneDescriptions.First();
            await OnFinishedLoading.InvokeAsync();

            SetProgress(9);
        }

        private bool SceneDescriptionReady = false;
        public void MarkSceneDescriptionReady()
        {
            // Populate Sprites
            SceneDescription.Sprites = SpriteObject.GetSprites(SceneDescription);

            SceneDescriptionForInterop = SceneDescription.GetSceneDescriptionForInterop();
            SceneDescriptionReady = true;
            StateHasChanged();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private bool SceneRequiresInitialization = true;
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && CurrentProgress == TotalProgress && SceneRequiresInitialization && SceneDescriptionReady)
            {
               await JSRuntime.InvokeVoidAsync("ThreeJSFunctions.load", SceneDescriptionForInterop);
               await SendDotNetInstanceToJS();
               SceneRequiresInitialization = false;
            }
        }

        private async Task SendDotNetInstanceToJS()
        {
            var dotNetObjRef = DotNetObjectReference.Create(this);
            await JSRuntime.InvokeVoidAsync("ThreeJSFunctions.setMapHandler", dotNetObjRef);
        }

        private async Task UpdateObjects(UniverseMap.SceneDescription UpdatedScene)
        {
            await JSRuntime.InvokeVoidAsync("ThressJSFunctions.update", UpdatedScene);
        }

        [JSInvokable]
        public async Task OnHover(string guid)
        {
            if (SceneDescription.Systems.ContainsKey(guid))
            {
                SystemHovered = SceneDescription.Systems[guid];
                await OnSystemHovered.InvokeAsync(SystemHovered);
            }

            if (SceneDescription.Sprites.ContainsKey(guid))
            {
                var SpriteHovered = SceneDescription.Sprites[guid];
                await OnSpriteHovered.InvokeAsync(SpriteHovered);
            }

            if (SceneDescription.InFlightShips.ContainsKey(guid))
            {
                var ShipHovered = SceneDescription.InFlightShips[guid];
                await OnShipHovered.InvokeAsync(ShipHovered);
            }
        }

        [JSInvokable]
        public async Task OnClick(string guid)
        {
            if (SceneDescription.Systems.ContainsKey(guid))
            {
                SystemClicked = SceneDescription.Systems[guid];
                await OnSystemClicked.InvokeAsync(SystemClicked);
            }

            if (SceneDescription.Sprites.ContainsKey(guid))
            {
                SpriteClicked = SceneDescription.Sprites[guid];
                await OnSpriteClicked.InvokeAsync(SpriteClicked);
            }

            if (SceneDescription.InFlightShips.ContainsKey(guid))
            {
                var ShipClicked = SceneDescription.InFlightShips[guid];
                await OnShipClicked.InvokeAsync(ShipClicked);
            }
        }
    }
}