﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Components;

using FIOWeb.JsonPayloads;
using FIOWeb.Models;


namespace FIOWeb.Components
{
	public enum SelectionEnum
	{
		Self,
		User,
		Group
	}

	public partial class UserOrGroup
	{
		[Parameter]
		public PermissionAllowance AllowancesRequired { get; set; }

		[Parameter]
		public EventCallback UserNameSelectionChanged { get; set; }

        [Parameter]
		public EventCallback GroupSelectionChanged { get; set; }

		[Parameter]
		public bool SupportsGroups { get; set; } = false;

        [Parameter]
		public string CurrentUserName 
		{
			get => _CurrentUserName;
			set
            {
				if (_CurrentUserName != value)
                {
					_CurrentUserName = value;
					CurrentUserNameChanged.InvokeAsync(value);
				}
            }
        }
        private string _CurrentUserName;

        [Parameter]
		public EventCallback<string> CurrentUserNameChanged { get; set; }

        [Parameter]
		public string CurrentGroup
		{
			get => _CurrentGroup;
			set
			{
				if (_CurrentGroup != value)
                {
					_CurrentGroup = value;
					CurrentGroupChanged.InvokeAsync(value);
				}
			}
		}
		private string _CurrentGroup;

        [Parameter]
		public EventCallback<string> CurrentGroupChanged { get; set; }

		public bool SelectionDrawerVisible { get; private set; }

		public SelectionEnum Selection { get; private set; }

		private List<string> UsersAllowedToView = new List<string>();

		private List<string> GroupsAllowedToView = new List<string>();

		protected override async Task OnInitializedAsync()
		{
			GlobalAppState.OnChange += StateHasChanged;
			await Refresh();
		}

		public void Dispose()
		{
			GlobalAppState.OnChange -= StateHasChanged;
		}

		private async Task Refresh()
		{
			var LoggedInUser = await GlobalAppState.GetUserName();

			if (NavManager.TryGetQueryString("UserName", out string OutCurrentUserName))
            {
				CurrentGroup = null;
				CurrentUserName = OutCurrentUserName;
				Selection = (CurrentUserName.ToUpper() == LoggedInUser.ToUpper()) ? SelectionEnum.Self : SelectionEnum.User;
			}
			else if (NavManager.TryGetQueryString("Group", out string OutCurrentGroup))
            {
				CurrentUserName = null;
				CurrentGroup = OutCurrentGroup;
				Selection = SelectionEnum.Group;
            }
			else
            {
				CurrentGroup = null;
				CurrentUserName = await GlobalAppState.GetUserName();
				Selection = SelectionEnum.Self;
            }

			UsersAllowedToView = GlobalAppState.PermissionAllowances.Where(pa => pa.Matches(AllowancesRequired)).Select(pa => pa.UserName).ToList();
			UsersAllowedToView.RemoveAll(u => u.Equals(CurrentUserName, StringComparison.OrdinalIgnoreCase)); // Remove the current ActiveUserName from the list
			UsersAllowedToView.Sort();
			if (CurrentUserName?.ToUpper() != (await GlobalAppState.GetUserName()).ToUpper())
			{
				UsersAllowedToView.Insert(0, await GlobalAppState.GetUserName()); // Add ourselves to the list
			}

			GroupMembership CurrentGroupDef = GroupMembership.Parse(CurrentGroup);
			GroupsAllowedToView = GlobalAppState.GroupMemberships
				.Where(gm => gm.GroupId != CurrentGroupDef.GroupId) // Don't include the active Group
				.OrderBy(gm => gm.GroupName)
				.Select(gm => $"{gm.GroupName}-{gm.GroupId}")
				.ToList();
		}

		private async void InternalUserNameSelectionChanged(string value)
        {
			SelectionDrawerVisible = false;
			await Task.Delay(500);
			
			NavManager.NavigateTo($"/{NavManager.GetRelativePathWithoutParams()}?UserName={value}");
			await UserNameSelectionChanged.InvokeAsync();
			await Refresh();
		}

		private async void InternalGroupSelectionChanged(string value)
        {
			SelectionDrawerVisible = false;
			await Task.Delay(500);

			NavManager.NavigateTo($"/{NavManager.GetRelativePathWithoutParams()}?Group={value}");
			await GroupSelectionChanged.InvokeAsync();
			await Refresh();
		}
	}
}