using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MatBlazor;
using Blazored.LocalStorage;
using Blazored.SessionStorage;
using CurrieTechnologies.Razor.Clipboard;
using BlazorDB;

namespace FIOWeb
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddMatBlazor();
            builder.Services.AddAntDesign();
            builder.Services.AddBlazoredLocalStorage(config => config.JsonSerializerOptions.WriteIndented = true);
            builder.Services.AddBlazoredSessionStorage(config => config.JsonSerializerOptions.WriteIndented = true);
            builder.Services.AddSingleton<GlobalAppState>();
            builder.Services.AddClipboard();
            builder.Services.AddMatToaster(config =>
            {
                config.Position = MatToastPosition.BottomRight;
                config.PreventDuplicates = true;
                config.NewestOnTop = true;
                config.ShowCloseButton = true;
                config.MaximumOpacity = 95;
                config.VisibleStateDuration = 3000;
            });

            DBRegister.Register("PlanetSearch", "PlanetNaturalId");
            DBRegister.Register("Materials-Minerals", "Ticker");
            DBRegister.Register("Materials-Ores", "Ticker");
            DBRegister.Register("Materials-Gases", "Ticker");
            DBRegister.Register("Materials-Liquids", "Ticker");
            DBRegister.Register("Materials-All", "Ticker");
            DBRegister.Register("Consumables-Basic", "Ticker");
            DBRegister.Register("Consumables-Luxury", "Ticker");
            DBRegister.Register("ComexExchanges", "ExchangeCode");
            DBRegister.Register("WorldSectors", "SectorId");
            DBRegister.Register("SystemStars", "NaturalId");
            DBRegister.Register("SystemLinks", "Left");
            DBRegister.Register("Stations", "ExchangeCode");
            DBRegister.Register("MapSceneDescription", "DummyKey");
            builder.Services.AddDB();

            var host = builder.Build();

            await host.RunAsync();
        }
    }
}
