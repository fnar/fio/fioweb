namespace FIOWeb.JsonPayloads
{
	public class DiscordIdPayload
	{
		public string DiscordId { get; set; }
	}
}