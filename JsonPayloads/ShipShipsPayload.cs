using System;
using System.Collections.Generic;

namespace FIOWeb.JsonPayloads
{
    public class Ship
    {
        public List<RepairMaterial> RepairMaterials { get; set; } = new List<RepairMaterial>();
        public List<AddressLine> AddressLines { get; set; } = new List<AddressLine>();
        public string ShipId { get; set; }
        public string StoreId { get; set; }
        public string StlFuelStoreId { get; set; }
        public string FtlFuelStoreId { get; set; }
        public string Registration { get; set; }
        public string Name { get; set; }
        public long CommissioningTimeEpochMs { get; set; }
        public string BlueprintNaturalId { get; set; }
        public string FlightId { get; set; }
        public float Acceleration { get; set; }
        public float Thrust { get; set; }
        public float Mass { get; set; }
        public float OperatingEmptyMass { get; set; }
        public float ReactorPower { get; set; }
        public float EmitterPower { get; set; }
        public float Volume { get; set; }
        public float Condition { get; set; }
        public int LastRepairEpochMs { get; set; }
        public string Location { get; set; }
        public float StlFuelFlowRate { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class RepairMaterial
    {
        public string ShipRepairMaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public int Amount { get; set; }
    }

    public class AddressLine
    {
        public string LineId { get; set; }
        public string LineType { get; set; }
        public string NaturalId { get; set; }
        public string Name { get; set; }
    }
}
