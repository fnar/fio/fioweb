using System.Collections.Generic;

namespace FIOWeb.JsonPayloads
{
    public class TickerRecipePayload
    {
        public string BuildingTicker { get; set; }
        public List<Input> Inputs { get; set; }
        public List<Output> Outputs { get; set; }
        public int DurationMs { get; set; }
        public string RecipeName { get; set; }
    }

    public class TickerRecipeInput
    {
        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public int Amount { get; set; }
    }

    public class TickerRecipeOutput
    {
        public string CommodityName { get; set; }
        public string CommodityTicker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
        public int Amount { get; set; }
    }
}