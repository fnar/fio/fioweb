namespace FIOWeb.JsonPayloads
{
    public class AllPlanetResult
    {
        public string PlanetNaturalId;
        public string PlanetName;
    }
}