using System;
using System.Collections.Generic;

namespace FIOWeb.JsonPayloads
{
    public class PlanetDataPayloadEx : PlanetDataPayload
    {
        public bool HasJobData { get; set; }

        public int? TotalPioneers { get; set; }
        public int? OpenPioneers { get; set; }

        public int? TotalSettlers { get; set; }
        public int? OpenSettlers { get; set; }

        public int? TotalTechnicians { get; set; }
        public int? OpenTechnicians { get; set; }

        public int? TotalEngineers { get; set; }
        public int? OpenEngineers { get; set; }

        public int? TotalScientists { get; set; }
        public int? OpenScientists { get; set; }

        public int? TotalPlots { get; set; }

        public List<int> DistanceResults { get; set; } = new List<int>();
    }

    public class PlanetDataPayload
    {
        public List<Resource> Resources { get; set; } = new List<Resource>();
        public List<BuildRequirement> BuildRequirements { get; set; } = new List<BuildRequirement>();
        public List<ProductionFee> ProductionFees { get; set; } = new List<ProductionFee>();
        public List<COGCProgram> COGCPrograms { get; set; } = new List<COGCProgram>();
        public List<COGCVote> COGCVotes { get; set; } = new List<COGCVote>();
        //public object[] COGCUpkeep { get; set; }
        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }
        public string Namer { get; set; }
        public long NamingDataEpochMs { get; set; }
        public bool Nameable { get; set; }
        public double Gravity { get; set; }
        public double MagneticField { get; set; }
        public double Mass { get; set; }
        public double MassEarth { get; set; }
        public double OrbitSemiMajorAxis { get; set; }
        public double OrbitEccentricity { get; set; }
        public double OrbitInclination { get; set; }
        public double OrbitRightAscension { get; set; }
        public double OrbitPeriapsis { get; set; }
        public int OrbitIndex { get; set; }
        public double Pressure { get; set; }
        public double Radiation { get; set; }
        public double Radius { get; set; }
        public double Sunlight { get; set; }
        public bool Surface { get; set; }
        public double Temperature { get; set; }
        public double Fertility { get; set; }
        public bool HasLocalMarket { get; set; }
        public bool HasChamberOfCommerce { get; set; }
        public bool HasWarehouse { get; set; }
        public bool HasAdministrationCenter { get; set; }
        public bool HasShipyard { get; set; }
        public string FactionCode { get; set; }
        public string FactionName { get; set; }
        public string GovernorId { get; set; }
        public string GovernorUserName { get; set; }
        public string GovernorCorporationId { get; set; }
        public string GovernorCorporationName { get; set; }
        public string GovernorCorporationCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string CollectorId { get; set; }
        public string CollectorName { get; set; }
        public string CollectorCode { get; set; }
        public double BaseLocalMarketFee { get; set; }
        public double LocalMarketFeeFactor { get; set; }
        public double WarehouseFee { get; set; }
        public string PopulationId { get; set; }
        public string COGCProgramStatus { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Resource
    {
        public string MaterialId { get; set; }
        public string ResourceType { get; set; }
        public double Factor { get; set; }
    }

    public class BuildRequirement
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }
        public int MaterialAmount { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }
    }

    public class ProductionFee
    {
        public string Category { get; set; }
        public double FeeAmount { get; set; }
        public string FeeCurrency { get; set; }
    }

    public class COGCProgram
    {
        public string ProgramType { get; set; }
        public long StartEpochMs { get; set; }
        public long EndEpochMs { get; set; }
    }

    public class COGCVote
    {
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public double Influence { get; set; }
        public string VoteType { get; set; }
        public long VoteTimeEpochMs { get; set; }
    }

}