using System;

namespace FIOWeb.JsonPayloads
{
	public class JsonAuthCreateAPIKeyPayload
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public bool AllowWrites { get; set; }
		public string Application { get; set; }
	}

	public class JsonAuthDeleteAPIKeyPayload
	{
		public string UserName { get; set; }
		public string Password { get; set; }
		public string ApiKeyToRevoke { get; set; }
	}

	public class JsonAuthAPIKeyPayload
	{
		public string AuthAPIKey { get; set; }
		public string Application { get; set; }
		public bool AllowWrites { get; set; }
		public DateTime LastAccessTime { get; set; }
	}

}