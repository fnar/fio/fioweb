using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWeb.Models;
using FIOWeb.JsonPayloads;

using MatBlazor;

namespace FIOWeb.Pages
{
    public partial class ShipmentFinder
    {
        private int ShipmentSearchPageSize = 50;

        private MatTheme blackTheme = new MatTheme()
        {
            Primary = "black",
            Secondary = "black"
        };

        private string SelectedSearchOption
        {
            get
            {
                return _SelectedSearchOption;
            }
            set
            {
                if (_SelectedSearchOption != value)
                {
                    _SelectedSearchOption = value;
                    StateHasChanged();
                    _ = GlobalAppState.LSSet_Generic<string>("ShipmentFinderSelectedSearchOption", _SelectedSearchOption);
                }
            }
        }
        private string _SelectedSearchOption = "Origin";

        private List<string> SearchOptions = new List<string>
        {
            "Origin",
            "Destination",
            "All"
        };

        private string PlanetSearchString
        {
            get
            {
                return _PlanetSearchString;
            }
            set
            {
                if (_PlanetSearchString != value)
                {
                    _PlanetSearchString = value;
                    StateHasChanged();
                    _ = GlobalAppState.LSSet_Generic<string>("ShipmentFinderPlanetSearchString", _PlanetSearchString);
                }
            }
        }
        private string _PlanetSearchString = null;

        private List<string> AllPlanetsAndCXs = null;

        private bool SearchLoading = false;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            if (await GlobalAppState.LS_ContainsKey("ShipmentFinderSelectedSearchOption"))
            {
                SelectedSearchOption = await GlobalAppState.LSGet_Generic<string>("ShipmentFinderSelectedSearchOption");
            }

            PlanetSearchString = await GlobalAppState.LSGet_Generic<string>("ShipmentFinderPlanetSearchString");

            var allPlanetsRequest = new Web.Request(HttpMethod.Get, "/planet/allplanets", await GlobalAppState.GetAuthToken());
            var allPlanetsResult = await allPlanetsRequest.GetResponseAsync<List<AllPlanetResult>>();
            if (allPlanetsResult != null)
            {
                allPlanetsResult = allPlanetsResult.OrderBy(apr => apr.PlanetNaturalId).ToList();
                AllPlanetsAndCXs = new List<string>();
                foreach (var res in allPlanetsResult)
                {
                    if (res.PlanetName != res.PlanetNaturalId)
                    {
                        AllPlanetsAndCXs.Add($"{res.PlanetName} ({res.PlanetNaturalId})");
                    }
                    else
                    {
                        AllPlanetsAndCXs.Add(res.PlanetNaturalId);
                    }
                }
            }

            var allExchangesRequest = new Web.Request(HttpMethod.Get, "/global/comexexchanges", await GlobalAppState.GetAuthToken());
            var allExchangesResult = await allExchangesRequest.GetResponseAsync<List<ExchangePayload>>();
            if (allExchangesResult != null)
            {
                allExchangesResult = allExchangesResult.OrderBy(aer => aer.LocationNaturalId).ToList();
                foreach (var exc in allExchangesResult)
                {
                    if (exc.LocationName != exc.LocationNaturalId)
                    {
                        AllPlanetsAndCXs.Add($"{exc.LocationNaturalId} ({exc.LocationName})");
                    }
                    else
                    {
                        AllPlanetsAndCXs.Add(exc.LocationNaturalId);
                    }
                }
            }


        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private List<ShipmentFinderModel> ShipmentSearchModels = new List<ShipmentFinderModel>();

        private async Task SearchClick()
        {
            SearchLoading = true;

            string searchString = PlanetSearchString;

            int openParenIdx = searchString.LastIndexOf('(');
            if (openParenIdx > 0)
            {
                searchString = searchString.Substring(0, openParenIdx - 1);
            }

            ShipmentFinderModel.ShipmentFinderSearchType searchType = ShipmentFinderModel.ShipmentFinderSearchType.All;
            if (SelectedSearchOption == "Origin")
            {
                searchType = ShipmentFinderModel.ShipmentFinderSearchType.Origin;
            }
            else if (SelectedSearchOption == "Destination")
            {
                searchType = ShipmentFinderModel.ShipmentFinderSearchType.Destination;
            }

            ShipmentSearchModels = await ShipmentFinderModel.GetShipmentFinderModels(searchString, searchType);

            SearchLoading = false;
        }

        private async Task OnClickLink(string PlanetNaturalId, string ContractNaturalId)
        {
            try
            {
                await Clipboard.WriteTextAsync($"LMA {PlanetNaturalId}/{ContractNaturalId}");
                Toaster.Add("Contract buffer command in clipboard", MatToastType.Info, "Copied");
            }
            catch
            {
                Toaster.Add("Browser failed to allow clipboard copying.", MatToastType.Danger, "Clipboard Failure");
            }
        }

        private void SortShipmentData(MatSortChangedEvent sort)
        {
            if (sort != null && sort.Direction != MatSortDirection.None && !String.IsNullOrWhiteSpace(sort.SortId))
            {
                bool bAscending = (sort.Direction == MatSortDirection.Asc);
                switch (sort.SortId)
                {
                    case "Link":
                        // Do nothing
                        break;
                    case "PlanetNaturalId":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.PlanetNaturalId).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.PlanetNaturalId).ToList();
                        break;
                    case "OriginPlanetNaturalId":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.OriginPlanetNaturalId).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.OriginPlanetNaturalId).ToList();
                        break;
                    case "DestinationPlanetNaturalId":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.DestinationPlanetNaturalId).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.DestinationPlanetNaturalId).ToList();
                        break;
                    case "JumpDistance":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.JumpDistance).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.JumpDistance).ToList();
                        break;
                    case "PayoutPrice":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.PayoutPrice).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.PayoutPrice).ToList();
                        break;
                    case "PayoutPricePer500":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.PayoutPricePer500).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.PayoutPricePer500).ToList();
                        break;
                    case "Weight":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.CargoWeight).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.CargoWeight).ToList();
                        break;
                    case "Volume":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.CargoVolume).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.CargoVolume).ToList();
                        break;
                    case "DeliveryTime":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.DeliveryTime).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.DeliveryTime).ToList();
                        break;
                    case "RatingDisplay":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.RatingDisplay).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.RatingDisplay).ToList();
                        break;
                    case "CreatorCompanyCode":
                        ShipmentSearchModels = bAscending ? ShipmentSearchModels.OrderBy(ssm => ssm.CreatorCompanyCode).ToList() : ShipmentSearchModels.OrderByDescending(ssm => ssm.CreatorCompanyCode).ToList();
                        break;
                }
            }
        }

        private void OnPlanetSearchStringChanged(string newValue)
        {
            PlanetSearchString = newValue;
            StateHasChanged();
        }
    }
}