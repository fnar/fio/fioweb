using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWeb.Models;
using FIOWeb.JsonPayloads;

using MatBlazor;
using Newtonsoft.Json;

namespace FIOWeb.Pages
{
    public partial class BurnRate
    {
        private PermissionAllowance RequiredAllowances = new PermissionAllowance()
        {
            StorageData = true,
            FlightData = true,
            BuildingData = true,
        };

        private class BurnRateExclusion
        {
            public string PlanetNaturalId { get; set; }
            public string MaterialTicker { get; set; }
        }

        private async Task BurnRateTable_OnRowClick(AntDesign.TableModels.RowData<BurnRateModel> model)
        {
            bool bAllowedToModifyExclusions = (ActiveUserName == await GlobalAppState.GetUserName());
            if (bAllowedToModifyExclusions)
            {
                if (!model.HasChildren)
                {
                    model.Data.IsInExclusionList = !model.Data.IsInExclusionList;

                    BurnRateExclusion excl = new BurnRateExclusion();
                    excl.PlanetNaturalId = model.Data.Parent.PlanetNaturalId;
                    excl.MaterialTicker = model.Data.Material;
                    bool bAddExclusion = model.Data.IsInExclusionList;
                    if (bAddExclusion)
                    {
                        var req = new Web.Request(HttpMethod.Post, "/usersettings/burnrate/addexclusion", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(excl));
                        _ = req.GetResultNoResponse();

                        var planetBRS = PlanetBurnRateSettings.Where(pbrs => pbrs.PlanetNaturalId == excl.PlanetNaturalId).FirstOrDefault();
                        if (planetBRS == null)
                        {
                            planetBRS = new PlanetBurnRateSettings();
                            PlanetBurnRateSettings.Add(planetBRS);
                        }

                        planetBRS.MaterialExclusions.Add(new MaterialExclusion { MaterialTicker = excl.MaterialTicker });
                    }
                    else
                    {
                        var req = new Web.Request(HttpMethod.Post, "/usersettings/burnrate/deleteexclusion", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(excl));
                        _ = req.GetResultNoResponse();

                        var planetBRS = PlanetBurnRateSettings.Where(pbrs => pbrs.PlanetNaturalId == excl.PlanetNaturalId).FirstOrDefault();
                        if (planetBRS != null)
                        {
                            planetBRS.MaterialExclusions.RemoveAll(me => me.MaterialTicker == excl.MaterialTicker);
                        }
                    }

                    UpdateModelYellowThresholds();
                    StateHasChanged();
                }
            }
        }

        private List<PlanetBurnRateSettings> PlanetBurnRateSettings = null;
        private List<BurnRateModel> BurnRateModels = null;
        private List<MaterialPayload> AllConsumables = null;

        private bool ShowHiddenConsumables
        {
            get
            {
                return _ShowHiddenConsumables;
            }
            set
            {
                if (_ShowHiddenConsumables != value)
                {
                    _ShowHiddenConsumables = value;
                    UpdateModelYellowThresholds();
                    StateHasChanged();
                    _ = GlobalAppState.LSSet_Generic<bool>("BurnRateShowHiddenConsumables", _ShowHiddenConsumables);
                }
            }
        }
        private bool _ShowHiddenConsumables = true;

        private const int ThresholdMin = 0;
        private const int ThresholdMax = 1000;
        private decimal YellowThreshold
        {
            get
            {
                return _YellowThreshold;
            }
            set
            {
                if (value <= RedThreshold)
                {
                    int red = (int)RedThreshold;

                    // Try to push yellow up first
                    int yellow = red;
                    yellow++;
                    if (yellow > ThresholdMax)
                    {
                        yellow--;
                        red--;
                    }

                    _YellowThreshold = yellow;
                    _RedThreshold = red;

                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateYellowThreshold", _YellowThreshold);
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateRedThreshold", _RedThreshold);
                }
                else
                {
                    _YellowThreshold = value;
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateYellowThreshold", _YellowThreshold);
                }

                UpdateModelYellowThresholds();
            }
        }
        private decimal _YellowThreshold = 15;

        private decimal RedThreshold
        {
            get
            {
                return _RedThreshold;
            }
            set
            {
                if (YellowThreshold <= value)
                {
                    int yellow = (int)YellowThreshold;

                    // Try to push red down first
                    int red = yellow;
                    red--;
                    if (red < ThresholdMin)
                    {
                        red++;
                        yellow++;
                    }

                    _RedThreshold = red;
                    _YellowThreshold = yellow;

                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateYellowThreshold", _YellowThreshold);
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateRedThreshold", _RedThreshold);

                    UpdateModelYellowThresholds();
                }
                else
                {
                    _RedThreshold = value;
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRateRedThreshold", _RedThreshold);
                }


            }
        }
        private decimal _RedThreshold = 5;

        private string ActiveUserName = null;

        protected override async Task OnInitializedAsync()
        {
            NavManager.TryGetQueryString<string>("UserName", out ActiveUserName);

            var basicConsumablesRequest = new Web.Request(HttpMethod.Get, "/material/category/consumables (basic)");
            AllConsumables = await basicConsumablesRequest.GetResponseAsync<List<MaterialPayload>>();

            var luxuryConsumablesRequest = new Web.Request(HttpMethod.Get, "/material/category/consumables (luxury)");
            AllConsumables = AllConsumables.Union(await luxuryConsumablesRequest.GetResponseAsync<List<MaterialPayload>>()).ToList();

            // Exclusions
            var exclusionsRequest = new Web.Request(HttpMethod.Get, $"/usersettings/burnrate/{ActiveUserName}", await GlobalAppState.GetAuthToken());
            PlanetBurnRateSettings = await exclusionsRequest.GetResponseAsync<List<PlanetBurnRateSettings>>();

            // Store in local store for now.  Will need to move these to server data eventually.
            if (await GlobalAppState.LS_ContainsKey("BurnRateYellowThreshold"))
            {
                _YellowThreshold = await GlobalAppState.LSGet_Generic<decimal>("BurnRateYellowThreshold");
            }

            if (await GlobalAppState.LS_ContainsKey("BurnRateRedThreshold"))
            {
                _RedThreshold = await GlobalAppState.LSGet_Generic<decimal>("BurnRateRedThreshold");
            }

            if (await GlobalAppState.LS_ContainsKey("BurnRateShowHiddenConsumables"))
            {
                ShowHiddenConsumables = await GlobalAppState.LSGet_Generic<bool>("BurnRateShowHiddenConsumables");
            }
        }

        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender && GlobalAppState.State == LoadState.Authenticated && BurnRateModels == null && AllConsumables != null)
            {
                if (ActiveUserName == null)
                {
                    ActiveUserName = await GlobalAppState.GetUserName();
                }

                await Refresh();
            }
        }

        private int ProgressCount = 0;
        private bool CannotSeeUsersData = false;

        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                ProgressCount = 0;
                CannotSeeUsersData = false;
                BurnRateModels = null;
                StateHasChanged();

                string AuthToken = await GlobalAppState.GetAuthToken();
                BurnRateModels = await BurnRateModel.GetBurnRateModels(ActiveUserName, AuthToken, AllConsumables, PlanetBurnRateSettings);
                if (BurnRateModels != null)
                {
                    UpdateModelYellowThresholds();
                }
                else
                {
                    CannotSeeUsersData = true;
                }
                ProgressCount = 1;
                StateHasChanged();
                InRefresh = false;
            }
        }

        private void UpdateModelYellowThresholds()
        {
            if (BurnRateModels != null)
            {
                foreach (var brm in BurnRateModels)
                {
                    brm.UpdateYellowThreshold((int)YellowThreshold);
                }
            }
        }

        private string GetFGColor(BurnRateModel model)
        {
            if (model != null && (model.Children == null || model.Children.Count == 0))
            {
                if (model.IsInExclusionList)
                {
                    return "lightgrey";
                }
            }

            return "black";
        }

        private string GetBackgroundColor(BurnRateModel model)
        {
            if (model != null && model.DaysRemaining != null && (model.BurnRate > 0 || model.Children.Count > 0) && !model.IsInExclusionList)
            {
                float DaysRemaining = (float)model.DaysRemaining;

                if (DaysRemaining < (float)RedThreshold)
                {
                    return "orangered";
                }
                if (DaysRemaining < (float)YellowThreshold)
                {
                    return "yellow";
                }
            }

            return "transparent";
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private async Task CopyPlanetBufferCommandToClipboard(string Storage)
        {
            if (!String.IsNullOrWhiteSpace(Storage) && Storage.Length >= 8)
            {
                string BufferCommand = $"INV {Storage.Substring(0, 8)}";
                try
                {
                    await Clipboard.WriteTextAsync(BufferCommand);
                    Toaster.Add("Copied storage buffer command to clipboard.", MatToastType.Info, "Copied");
                }
                catch
                {
                    Toaster.Add("Browser failed to allow clipboard copying.", MatToastType.Danger, "Clipboard Failure");
                }
            }
        }
    }
}