using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using MatBlazor;
using Microsoft.JSInterop;

using FIOWeb.UniverseMap;

namespace FIOWeb.Pages
{
    public partial class Map
    {
        private Components.MapComponent MapComponent { get; set; }

        private UniverseMap.SystemObject SystemHovered { get; set; }
        private UniverseMap.SystemObject SystemClicked { get; set; }

        private UniverseMap.SpriteObject SpriteHovered { get; set; }
        private UniverseMap.SpriteObject SpriteClicked { get; set; }

        public string SpriteHoveredDisplay
        {
            get
            {
                if (SpriteHovered != null)
                {
                    string Prefix = "";
                    switch(SpriteHovered.SpriteObjType)
                    {
                        case (int)SpriteObjectType.CommodityExchange:
                            Prefix = "CX:";
                            break;
                        case (int)SpriteObjectType.LocalMarket:
                            Prefix = "LMs:";
                            break;
                        case (int)SpriteObjectType.Shipyard:
                            Prefix = "SHYs:";
                            break;
                    }
                    return Prefix + " " + string.Join(", ", SpriteHovered.SpriteNaturalIdentifiers);
                }

                return string.Empty;
            }
        }

        private int PlanetDescriptionsPageSize = 50;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            await Task.FromResult(0);
        }

        private void SetSystemHovered(UniverseMap.SystemObject sys)
        {
            SpriteHovered = null;
            SystemHovered = sys;
            StateHasChanged();
        }

        private void SetSystemClicked(UniverseMap.SystemObject sys)
        {
            SpriteClicked = null;
            SystemClicked = sys;
            StateHasChanged();
        }

        private void SetSpriteHovered(UniverseMap.SpriteObject sprite)
        {
            SystemHovered = null;
            SpriteHovered = sprite;
            StateHasChanged();
        }

        private void FinishedLoading()
        {
            MapComponent.SceneDescription.Sprites = UniverseMap.SpriteObject.GetSprites(MapComponent.SceneDescription);
            MapComponent.MarkSceneDescriptionReady();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }
    }
}