﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using FIOWeb.Models;
using FIOWeb.JsonPayloads;

namespace FIOWeb.Pages
{
    public partial class Burn
    {
        private int ProgressCount = 0;

        private List<BurnPayload> BurnPayload = null;
        private List<BurnModel> BurnModels = new List<BurnModel>();

        private List<MaterialPayload> AllMaterials = null;

        private IEnumerable<string> AllAvailableUsers { get; set; } = new List<string>();
        private IEnumerable<string> FilteredUsers { get; set; } = new List<string>();

        private IEnumerable<string> AllAvailableTickers { get; set; } = new List<string>();
        private IEnumerable<string> FilteredTickers { get; set; } = new List<string>();

        private IEnumerable<string> AllAvailablePlanets { get; set; } = new List<string>();
        private IEnumerable<string> FilteredPlanets { get; set; } = new List<string>();

        private string ActiveUserName = null;
        private string ActiveGroup = null;

        private PermissionAllowance RequiredAllowances = new PermissionAllowance()
        {
            StorageData = true,
            BuildingData = true,
            ProductionData = true,
            WorkforceData = true,
        };

        private string DisplayMode
        {
            get
            {
                return _DisplayMode;
            }
            set
            {
                if (_DisplayMode != value)
                {
                    _DisplayMode = value;
                    BurnModelShow bms = BurnModelShow.ShowAll;
                    if (_DisplayMode == "ShowAll")
                    {
                        bms = BurnModelShow.ShowAll;
                    }
                    else if (_DisplayMode == "ShowOnlyWorkforce")
                    {
                        bms = BurnModelShow.ShowOnlyWorkforce;
                    }
                    else if (_DisplayMode == "ShowOnlyProduction")
                    {
                        bms = BurnModelShow.ShowOnlyProduction;
                    }
                    else
                    {
                        Debug.Assert(false);
                    }

                    BurnModels.ForEach(bm => bm.Show = bms);

                    StateHasChanged();
                }
            }
        }
        private string _DisplayMode = "ShowAll";

        protected override async Task OnInitializedAsync()
        {
            if (NavManager.TryGetQueryString<string>("UserName", out string OutActiveUserName))
            {
                ActiveUserName = OutActiveUserName;
            }
            else if (NavManager.TryGetQueryString<string>("Group", out string OutActiveGroup))
            {
                ActiveGroup = OutActiveGroup;
            }
            else
            {
                ActiveUserName = await GlobalAppState.GetUserName();
            }

            // Store in local store for now.  Will need to move these to server data eventually.
            if (await GlobalAppState.LS_ContainsKey("BurnYellowThreshold"))
            {
                _YellowThreshold = await GlobalAppState.LSGet_Generic<decimal>("BurnYellowThreshold");
            }

            if (await GlobalAppState.LS_ContainsKey("BurnRedThreshold"))
            {
                _RedThreshold = await GlobalAppState.LSGet_Generic<decimal>("BurnRedThreshold");
            }

            var allMaterialsRequest = new Web.Request(HttpMethod.Get, "/material/allmaterials");
            AllMaterials = await allMaterialsRequest.GetResponseAsync<List<MaterialPayload>>();

            await Refresh();
        }

        protected override void OnInitialized()
        {
            GlobalAppState.OnChange += StateHasChanged;
        }

        private bool CannotSeeUsersData = false;

        private bool InRefresh = false;
        private async Task Refresh()
        {
            if (!InRefresh)
            {
                InRefresh = true;
                ProgressCount = 0;
                CannotSeeUsersData = false;
                BurnModels = null;
                _DisplayMode = "ShowAll";
                StateHasChanged();

                string AuthToken = await GlobalAppState.GetAuthToken();
                var CurrentGroup = GroupMembership.Parse(ActiveGroup);
                BurnPayload = await BurnModel.GetBurnPayload(ActiveUserName, null, CurrentGroup.GroupId, AuthToken);
                BurnPayload = BurnPayload.Where(bp => string.IsNullOrEmpty(bp.Error)).ToList();

                PopulateFilters();

                ApplyFilters();

                if (BurnModels == null)
                {
                    CannotSeeUsersData = true;
                }

                ProgressCount = 1;

                StateHasChanged();
                InRefresh = false;
            }
        }

        public void PopulateFilters()
        {
            if (BurnPayload != null)
            {
                AllAvailableUsers = BurnPayload
                    .Select(bp => bp.UserName.ToUpper())
                    .Distinct()
                    .OrderBy(user => user)
                    .ToList();

                AllAvailablePlanets = BurnPayload
                    .Select(bp => bp.PlanetName.ToUpper())
                    .Distinct()
                    .OrderBy(user => user)
                    .ToList();

                AllAvailableTickers = AllMaterials
                    .Select(m => m.Ticker.ToUpper())
                    .OrderBy(ticker => ticker)
                    .ToList();
            }
        }

        public async void ApplyFilters()
        {
            await Task.FromResult(0);
            BurnModels = BurnModel.GetBurnModels(BurnPayload, FilteredUsers, FilteredTickers, FilteredPlanets);
            StateHasChanged();
        }

        public void RebuildTable()
        {
            StateHasChanged();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private const int ThresholdMin = 0;
        private const int ThresholdMax = 1000;
        private decimal YellowThreshold
        {
            get
            {
                return _YellowThreshold;
            }
            set
            {
                if (value <= RedThreshold)
                {
                    int red = (int)RedThreshold;

                    // Try to push yellow up first
                    int yellow = red;
                    yellow++;
                    if (yellow > ThresholdMax)
                    {
                        yellow--;
                        red--;
                    }

                    _YellowThreshold = yellow;
                    _RedThreshold = red;

                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnYellowThreshold", _YellowThreshold);
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRedThreshold", _RedThreshold);
                }
                else
                {
                    _YellowThreshold = value;
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnYellowThreshold", _YellowThreshold);
                }
            }
        }
        private decimal _YellowThreshold = 15;

        private decimal RedThreshold
        {
            get
            {
                return _RedThreshold;
            }
            set
            {
                if (YellowThreshold <= value)
                {
                    int yellow = (int)YellowThreshold;

                    // Try to push red down first
                    int red = yellow;
                    red--;
                    if (red < ThresholdMin)
                    {
                        red++;
                        yellow++;
                    }

                    _RedThreshold = red;
                    _YellowThreshold = yellow;

                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnYellowThreshold", _YellowThreshold);
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRedThreshold", _RedThreshold);
                }
                else
                {
                    _RedThreshold = value;
                    _ = GlobalAppState.LSSet_Generic<decimal>("BurnRedThreshold", _RedThreshold);
                }
            }
        }
        private decimal _RedThreshold = 5;

        private string GetBackgroundColor(double DaysUntilExhaustion)
        {
            if (DaysUntilExhaustion < (double)RedThreshold)
            {
                return "orangered";
            }
            if (DaysUntilExhaustion < (float)YellowThreshold)
            {
                return "yellow";
            }
            else
            {
                return "transparent";
            }
        }

        private string GetUnitsToYellow(BurnModel model)
        {
            if (model.DaysUntilExhaustion < (double)YellowThreshold && model.Delta < 0.0)
            {
                var unitsToYellow = Math.Abs(((double)YellowThreshold - model.DaysUntilExhaustion) * (double)model.Delta);
                return $"{unitsToYellow:N2}";
            }

            return String.Empty;
        }
    }
}